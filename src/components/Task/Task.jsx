import './Task.scss';
import React, { Component } from 'react';
import EditForm from '../EditForm/EditForm';
import TasksRequests from '../../requests/tasksRequests';

class Task extends Component {
    constructor (props) {
        super(props);
        this.state = {
            isUpdating: false
        };
    }
    handleEditTaskClick = () => {
        this.setState({isUpdating: true});
    };
    handleDeleteTaskClick = () => {
        TasksRequests.delTask(this.props.task).then((response) => {
            this.props.onTaskUpdated(response, 'delete');
        });
    };
    handleSaveNewContentClick = (value) => {
        TasksRequests.updateTask(this.props.task, value).then((response) => {
            this.setState({isUpdating: false});
            this.props.onTaskUpdated(response, 'update');
        });
    };
    handleCancelNewContentClick = () => {
        this.setState({isUpdating: false});
    };
    handleCheckTask = () => {
        TasksRequests.changeTaskState(this.props.task).then((response) => {
            this.props.onTaskUpdated(response, 'update');
        });
    };
    handleTaskUp = () => {
        TasksRequests.upTaskPosition(this.props.task).then((response) => {
            this.props.onTaskUpdated(response, 'update');
        });
    };
    handleTaskDown = () => {
        TasksRequests.downTaskPosition(this.props.task).then((response) => {
            this.props.onTaskUpdated(response, 'update');
        });
    };
    render () {
        return (
            this.state.isUpdating
                ? <EditForm
                    defaultValue={this.props.task.content}
                    callbackSaveClick={this.handleSaveNewContentClick}
                    callbackCancelClick={this.handleCancelNewContentClick}
                />
                : <div className='Task'>
                    <div className='left-area'>
                        <div className="checkbox-wrapper">
                            <input className='done-state' type='checkbox' checked={this.props.task.is_done} onChange={this.handleCheckTask}/>
                        </div>
                        <text className='task-content'>{this.props.task.content} </text>
                    </div>
                    <div className="right-area">
                        <div className="pos-buttons">
                            <div className="arrow-up-wrap">
                                <button className="arrow up" onClick={this.handleTaskUp}>u</button>
                            </div>
                            <div className="arrow-down-wrap">
                                <button className="arrow down" onClick={this.handleTaskDown}>d</button>
                            </div>
                        </div>
                        <div className="edit-del-buttons">
                            <div className="edit-btn-wrap">
                                <button className='edit-btn' onClick={this.handleEditTaskClick}>E</button>
                            </div>
                            <div className="delete-btn-wrap">
                                <button className='delete-btn' onClick={this.handleDeleteTaskClick}>X</button>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}
export default Task;
